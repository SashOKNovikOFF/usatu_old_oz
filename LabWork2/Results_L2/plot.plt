set terminal wxt persist
set terminal png size 1024, 1024
set output 'S_01_P_001.png'

set xlabel "x"
set ylabel "f(x)"

plot "S_01_P_001.txt" using 1:3 with lines ti "Точное решение", \
     "S_01_P_001.txt" using 1:2 with lines ti "Приближённое решение"