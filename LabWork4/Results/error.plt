reset
set terminal wxt persist

#set yrange [0:2]

set xlabel "Число итераций"
set ylabel "Норма между точным и численным решениями"

plot "Error.txt" using 1 with lines notitle