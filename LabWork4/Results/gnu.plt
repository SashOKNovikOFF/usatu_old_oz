reset
set terminal wxt persist

set yrange [0:2]

set xlabel "OX"
set ylabel "phi(x)"

plot "Solution.txt" using 1:2 with lines ti "Приближённое решение", \
     "Solution.txt" using 1:3 with lines ti "Точное решение"