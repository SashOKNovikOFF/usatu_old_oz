#define _USE_MATH_DEFINES

#include <cmath>
#include <vector>
#include <iostream>
#include <random>
#include "printOut.hpp"

// ���������� � �������� ��������� ���������
const double aBig   = 1.0;
const double aSmall = 1.0;
const double length = 1.0;
const double t0 = 0.01;
const int numSeries = 300;

// ������ �����
const int numNodes = 2001;
const double dx = length / (numNodes - 1);

// ��������� ��������� �����
std::default_random_engine generator;
std::uniform_real_distribution<double> distribution(-1.0, 1.0);
const double delta = 0.1;

// ������ ������� ��������� ���������
double exactSolution(double x, double t)
{
	double sum = 0.0;
	for (int n = 1; n <= numSeries; n++)
	{
		double lambda = M_PI * n / length;
		
		double add_1 = 2.0 / length * cos(lambda * aSmall * t);
		double add_2 = 2.0 / aSmall / M_PI / n * sin(lambda * aSmall * t);
		double numer = aSmall * (sinh(length / aSmall) * cos(lambda * length) + aSmall * lambda * cosh(length / aSmall) * sin(lambda * length));
		double denom = pow(aSmall * lambda, 2.0) + 1.0;
		
		sum += add_1 /*(add_1 - add_2)*/ * numer / denom * cos(lambda * x);
	};
	sum = aBig * pow(aSmall, 2.0) / length /* * (1.0 - t) */ + aBig * aSmall / sinh(length / aSmall) * sum;
	
	return sum;
};

// ������ ������� ���������� ������� phi(x)
double phiSolution(double x)
{
	return aBig * aSmall * cosh(x / aSmall) / sinh(length / aSmall);
};

// ���������� ������ ����� ���������
double rightPart(double x, double t)
{
	double add = aBig * pow(aSmall, 2.0) * t / length;
	
	double series = 0.0;
	for (int n = 1; n < numSeries; n++)
	{
		double lambda = M_PI * n / length;
		double BN = - aBig * aSmall / sinh(length / aSmall) * 2.0 / aSmall / M_PI / n * cos(lambda * x) * sin(lambda * aSmall * t);
		
		double numer = aSmall * (sinh(length / aSmall) * cos(lambda * length) + aSmall * lambda * cosh(length / aSmall) * sin(lambda * length));
		double denom = pow(aSmall * lambda, 2.0) + 1.0;
		double CN = numer / denom;
		
		series += BN * CN;
	};
	
	return exactSolution(x, t) * (1.0 + delta * distribution(generator)) /* + add - series */ ;
};

// ������ ��������� phi(xi) * dxi
double calcIntPHI(std::vector<double>& phiVector)
{
	double intPHI = 0.0;
	intPHI += phiVector[0           ] * dx / 2.0;
	intPHI += phiVector[numNodes - 1] * dx / 2.0;
	for (int node = 1; node < numNodes - 1; node++)
		intPHI += phiVector[node] * dx;
	
	return intPHI;
};

// ������ ��������� phi(xi) * cos(lambda * xi) * dxi
void calcIntPHICOS(std::vector<double>& intLambdaVector, std::vector<double>& xNodes, std::vector<double>& phiVector)
{
	for (int n = 1; n < numSeries; n++)
	{
		double lambda = M_PI * n / length;
		
		double intPHICOS = 0.0;
		intPHICOS += phiVector[0           ] * cos(lambda * xNodes[0           ]) * dx / 2.0;
		intPHICOS += phiVector[numNodes - 1] * cos(lambda * xNodes[numNodes - 1]) * dx / 2.0;
		for (int node = 1; node < numNodes - 1; node++)
			intPHICOS += phiVector[node] * cos(lambda * xNodes[node]) * dx;
		
		intLambdaVector[n] = intPHICOS;
	};
};

// ���������� ����� ����� ���������
double leftPart(double x, double t, std::vector<double>& xNodes, std::vector<double>& phiVector)
{
	// ������ ��������� phi(xi) * dxi
	double intPHI = 0.0;
	intPHI += phiVector[0           ] * dx / 2.0;
	intPHI += phiVector[numNodes - 1] * dx / 2.0;
	for (int node = 1; node < numNodes - 1; node++)
		intPHI += phiVector[node] * dx;
	
	// ������ ��������� phi(xi) * cos(lambda * xi) * dxi
	double series = 0.0;
	for (int n = 1; n < numSeries; n++)
	{
		double lambda = M_PI * n / length;
		
		double intPHICOS = 0.0;
		intPHICOS += phiVector[0           ] * cos(lambda * xNodes[0           ]) * dx / 2.0;
		intPHICOS += phiVector[numNodes - 1] * cos(lambda * xNodes[numNodes - 1]) * dx / 2.0;
		for (int node = 1; node < numNodes - 1; node++)
			intPHICOS += phiVector[node] * cos(lambda * xNodes[node]) * dx;
		
		series += intPHICOS * cos(lambda * aSmall * t) * cos(lambda * x);
	};
	
	return 1.0 / length * intPHI + 2.0 / length * series;
};

// ���������� ����� ����� ��������� (���������� ������)
double leftPartFAST(double x, double t, double intPHI, std::vector<double>& intLambdaVector)
{
	double series = 0.0;
	for (int n = 1; n < numSeries; n++)
	{
		double lambda = M_PI * n / length;
		
		double intPHICOS = intLambdaVector[n];
		series += intPHICOS * cos(lambda * aSmall * t) * cos(lambda * x);
	};
	
	return 1.0 / length * intPHI + 2.0 / length * series;
};

// ������ ����� ��������� � ������ ���������
double getError(std::vector<double>& vectorA, std::vector<double>& vectorB)
{
	double errorMax = 0.0;
	for (std::size_t node = 0; node < vectorA.size(); node++)
		errorMax = pow(vectorA[node] - vectorB[node], 2.0);
	
	return sqrt(errorMax);
};

int main()
{
	// ������� ����� ����� �� ��� OX
	std::vector<double> xNodes(numNodes);
	for (int node = 0; node < numNodes; node++)
		xNodes[node] = 0.0 + node * dx;
	
	// ������� ������� phi(x)
	std::vector<double> phiVector(numNodes);
	for (int node = 0; node < numNodes; node++)
		phiVector[node] = phiSolution(xNodes[node]);
	
	// �������� ���������� ����� � ������ ������ ���������
	std::cout << rightPart(0.3, 0.5) - leftPart(0.3, 0.5, xNodes, phiVector) << std::endl;
	std::cout << rightPart(0.8, 0.3) - leftPart(0.8, 0.3, xNodes, phiVector) << std::endl;
	std::cout << rightPart(0.5, 0.1) - leftPart(0.5, 0.1, xNodes, phiVector) << std::endl;
	
	/*// ������ �������� ����� ��������� A
	double normSqrA = 0.0;
	std::vector<double> normVector(numNodes, 1.0);
	std::vector<double> tempRPVector(numNodes, 1.0);
	// ��������� ������� (������� ����������)
	double tempIntPHI = calcIntPHI(normVector);
	std::vector<double> tempLambdaVector(numSeries);
	calcIntPHICOS(tempLambdaVector, xNodes, normVector);
	for (int node = 0; node < numNodes; node++)
		tempRPVector[node] = leftPartFAST(xNodes[node], t0, tempIntPHI, tempLambdaVector);
	// ������� ��������� � ������������ L2[0, l]
	normSqrA += pow(tempRPVector[0], 2.0) * dx / 2.0;
	normSqrA += pow(tempRPVector[numNodes - 1], 2.0) * dx / 2.0;
	for (int node = 1; node < numNodes - 1; node++)
		normSqrA += pow(tempRPVector[node], 2.0) * dx;
	std::cout << normSqrA << std::endl;*/
	
	// �������� ����������� �������������
	double regParam = 1.0E-1;
	std::cout << "R parameter: " << regParam << std::endl;
	
	// ������ ����� ������������� ���������
	std::vector<double> fVector(numNodes);
	for (int node = 0; node < numNodes; node++)
		fVector[node] = rightPart(xNodes[node], t0);
	
	// ������ � ����� �������� ������� phi(x)
	std::vector<double> oldSolution(numNodes, 0.0);
	std::vector<double> newSolution(numNodes, 0.0);
	
	// ������ ����� ������ � ����������� ���������
	double error = getError(oldSolution, phiVector);
	std::vector<double> errorVec = { error };
	
	// ����� ����������� ������������� ��������
	for (int time = 0; time < 100; time++)
	{	
		// ��������� ������� (������� ����������)
		double intPHI = calcIntPHI(oldSolution);
		std::vector<double> intLambdaVector(numSeries);
		calcIntPHICOS(intLambdaVector, xNodes, oldSolution);

		for (int node = 0; node < numNodes; node++)		
			newSolution[node] = oldSolution[node] + regParam * (fVector[node] - leftPartFAST(xNodes[node], t0, intPHI, intLambdaVector));
		
		oldSolution = newSolution;
		
		if (error > getError(newSolution, phiVector))
			error = getError(newSolution, phiVector);
		else
			break;
		
		errorVec.push_back(getError(newSolution, phiVector));
	};
	
	std::vector< std::vector<double> > data = { xNodes, newSolution, phiVector };
	printOutWithoutHeader("Solution.txt", 10, data, false);
	std::vector< std::vector<double> > data0 = { xNodes, fVector };
	printOutWithoutHeader("RightPart.txt", 10, data0, false);
	
	printOutOnlyData("Error.txt", 10, errorVec);
	
	return 0;
};